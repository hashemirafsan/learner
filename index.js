const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const app = express();
const PORT = process.env.PORT || 8200;
const Question = require("./models/questions");
const Answer = require("./models/answers");

// mongoose.connect("mongodb://127.0.0.1:27017/qplex");
// mongoose.connection.once("open", () => {
//     return null;
// });

mongoose.connect("mongodb://hashemirafsan:01625903501RrR@ds115762.mlab.com:15762/graphql");
mongoose.connection.once('open', () => {
    console.log('Connect To Mlab');
})

app.use(cors());

const response = (res, message, sensor) => {
    res.json( 
        {
            status: 200,
            message,
            sensor
        }
    );
}

app.get('/api/question', (req, res) => {
    Question.findOne({
        text: req.query.question
    }, (err, question) => {
        if (err) {
            throw new Error("Something's going wrong");
        }

        if(!question) {
            question = new Question({
                text: req.query.question
            });

            question.save();

            const sensor = {
                type: "answer",
                q_id: question._id
            }

            return response(
                res, 
                "Thanks for the question, but I don't know the answer to it. Will you please tell me the answer to train me?",
                sensor
            );
        } else {
            Answer.find({ question_id: question._id }, (err, answer) => {
                if (err) {
                    throw new Error("Find some problem")
                }

                if (answer.length) {
                    
                    const sort = answer.sort((a, b) => {
                        return b.percentage - a.percentage;
                    });

                    const sensor = {
                        type: "question"
                    }

                    return response(
                        res,
                        sort,
                        sensor
                    );
                } else {
                    const sensor = {
                        type: "answer",
                        q_id: question._id
                    }
                    return response(
                        res,
                        "Familiar question for me but I don't know the answer to it. Will you tell me the answer?",
                        sensor
                    );
                }
            });
        }
    });
});


app.get('/api/answer', (req, res) => {

    Answer.findOne({
        text: req.query.answer,
        question_id: req.query.question_id
    }, (err, answer) => {
        if (err) {
            throw new Error("Something happening!")
        }

        if (answer) {
            const answer_q_id = answer.question_id;
            Question.findById(answer_q_id, (err1, questions) => {
                let count = questions.total_answer + 1;
                Question.findByIdAndUpdate(questions._id, {
                    $set: {
                        total_answer: count
                    }
                }, (err2, question2) => {
                    //console.log(answer)
                    const id = answer._id;
                    Answer.findById(id, (err3, answer2) => {
                        //console.log(answer2)
                        let countAnswer = answer2.already + 1;
                        Answer.findByIdAndUpdate(answer2._id, 
                            {
                                $set: {
                                    already: countAnswer
                                }
                            }
                            ,(err4, answer3) => {
                                //console.log(question2)
                                Question.findById(answer3.question_id, (err5, question3) => {
                                    Answer.find({ question_id: question3._id }, (err6, answer6) => {
                                        //console.log(answer6)
                                        answer6.map((item) => {
                                            const per = (item.already / question3.total_answer) * 100;
                                            Answer.findByIdAndUpdate(item._id, {
                                                $set: {
                                                    percentage: per 
                                                }
                                            }, (err7, answer7) => {
                                                console.log(answer7);
                                            });
                                        })
                                    })
                                })
                        })
                    });
                });
            });

            Answer.find({ question_id: answer.question_id }, (err, answer7) => {
                const sort = answer7.sort((a, b) => {
                    return b.percentage - a.percentage;
                });
                const sensor = {
                    type: "question"
                }
                return response(
                    res,
                    sort,
                    sensor
                );
            })

        } else {
            answer = new Answer({
                text: req.query.answer,
                question_id: req.query.question_id
            });

            answer.save();
            const sensor = {
                type: "question"
            }

            return response(res,
                "Got it! Train me more, ask me another question!",
                sensor
            );
        }
    })
});

app.listen(PORT,() => {
    return null;
});
