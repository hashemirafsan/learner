const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const AnswerSchema = new Schema({
    text: {
        type: String,
        required: true
    },
    question_id: {
        type: String,
        required: true
    },
    already: {
        type: Number,
        default: 0
    },
    percentage: {
        type: Number,
        default: 0
    }
});

module.exports = mongoose.model('Answer', AnswerSchema);