const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const QuestionSchema = new Schema({
    text: {
        type: String,
        required: true
    },
    total_answer: {
        type: Number,
        default: 0
    }
});

module.exports = mongoose.model('Question', QuestionSchema);