# Learner

**Learner** is a simple api for teaching your bot

## Installation
https://learner.now.sh/api/ 

## Usage

Now here we have two GET methods to complete our work.

* [/question](#question)
* [/answer](#answer)

### `/question?question=:question`

When you want set your question or statement.

```javascript
response {
    res,
    "Thanks for the question, but I don't know the answer to it. Will you please tell me the answer to train me?",
    sensor
}
```

### `/answer?answer=:answer&question_id=:question_id`
When you want to set answer for each question through question id.

```javascript
response {
    res,
    "Got it! Train me more, ask me another question!",
    sensor
}
```

## Bugs and Issues

If you encounter any bugs or issues, feel free to [open an issue at
gitlab](https://gitlab.com/hashemirafsan/learner/issues).

## Contribution
It is open source project so you can clone, fork and can join to build it up. I use mongoDB for storing database if your data is confidential be careful to use database. 